$(function(){
//ウィンドウサイズによる画像置換

  var sizeChange = $('.switch'), // 置換の対象とするclass属性。
  pcName = '_pc', // 置換の対象とするsrc属性の末尾の文字列
  spName = '_sm',// 置換の対象とするsrc属性の末尾の文字列
  replaceWidth = 641; // 画像を切り替えるウィンドウサイズ。

  sizeChange.each(function(){
  var $this = $(this);
  function imgSize(){
  if(window.innerWidth > replaceWidth) { // ウィンドウサイズが641px以上であれば_spを_pcに置換する。
   $this.attr('src',$this.attr('src').replace(spName,pcName));
   } else {
      $this.attr('src',$this.attr('src').replace(pcName,spName));
			}
		}
		$(window).resize(function(){imgSize();});
	imgSize();
	});
});