function initialize() {
  var latlng = new google.maps.LatLng(35.691494, 139.692633);
  var myOptions = {
    zoom: 18,/*拡大比率*/
    center: latlng,/*表示枠内の中心点*/
    mapTypeControlOptions: { mapTypeIds: ['sample', google.maps.MapTypeId.ROADMAP] }/*表示タイプの指定*/
  };
  var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

  /*アイコン設定▼*/
  var icon = new google.maps.MarkerImage('js/yubi3.png',
    new google.maps.Size(50,89),/*アイコンサイズ設定*/
    new google.maps.Point(0,0)/*アイコン位置設定*/
    );
  var markerOptions = {
    position: latlng,
    map: map,
    icon: icon,
    title: 'BEC Inc.'
  };
  var marker = new google.maps.Marker(markerOptions);
  　/*アイコン設定ここまで▲*/

  /*取得スタイルの貼り付け*/
  var styleOptions = [
  {
    "elementType": "undefined",
    "stylers": [
    { "hue": "#00afea" },
    { "lightness": 23 },
    { "gamma": 0.76 },
    { "saturation": -6 }
    ]
  }
  ];
var styledMapOptions = { name: 'BEC Inc.' }
var sampleType = new google.maps.StyledMapType(styleOptions, styledMapOptions);
map.mapTypes.set('sample', sampleType);
map.setMapTypeId('sample');
}
google.maps.event.addDomListener(window, 'load', initialize);